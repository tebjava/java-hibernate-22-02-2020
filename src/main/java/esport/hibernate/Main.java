package esport.hibernate;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class Main {
	
	protected SessionFactory sessionFactory;
	
	protected void setup() {
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
		        .configure() // configures settings from hibernate.cfg.xml
		        .build();
		try {
		    sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception ex) {
		    StandardServiceRegistryBuilder.destroy(registry);
		}
    }
 
    protected void exit() {
    	if (sessionFactory != null) {
    		sessionFactory.close();
    		System.out.println("Zakonczenie");
    	}
    }
 
    protected void create() {
        // code to save a book
    	Session s = sessionFactory.openSession();
    	s.beginTransaction();
    	Osoby o = new Osoby();
    	o.setName("Marianna");
    	o.setSurname("Wilczkiewicz");
    	o.setNick("pantera");
    	o.setEmail("niemam@nazwa.pl");
    	o.setDate("1995-10-25");
    	o.setTelephone("777-888-999");
    	s.save(o);
    	s.getTransaction().commit();
    	s.close();
    }
 
    protected void read() {
        // code to get a book
    	Session s = sessionFactory.openSession();
    	//s.beginTransaction();
    	
    	Osoby o = (Osoby) s.get(Osoby.class, Long.parseLong("1"));
    	//s.getSession().createCriteria(Osoby.class).list();
    	System.out.println(o.getName() + " " + o.getSurname());
    	
    	//s.getTransaction().commit();
    	s.close();
    }
    
    protected void getAll() {
    	Session s = sessionFactory.openSession();
    	CriteriaBuilder builder = s.getCriteriaBuilder();
    	CriteriaQuery<Osoby> criteria = builder.createQuery(Osoby.class);
        criteria.from(Osoby.class);
        List<Osoby> osoby = s.createQuery(criteria).getResultList();
        s.close();
        for (Osoby o : osoby) {
			System.out.println(o.getName() + " " + o.getSurname());
		}
    }
 
    protected void update() {
        // code to modify a book
    	Session s = sessionFactory.openSession();
    	
    	
    }
 
    protected void delete() {
        // code to remove a book
    }
	
	public static void main(String[] s) {
		System.out.println("Test aplikacji hibernate");
		Main hibernate = new Main();
		hibernate.setup();
		//zapis do bazy danych
		//hibernate.create();
		//odczyt z bazy danych
		//hibernate.read();
		hibernate.getAll();
		hibernate.exit();
		System.out.println(Osoby.class);
	}
}
